*  ---- Todo ----
1. nhập, xuất thông tin cho kho.
2. nhập, xuất thông tin máy.
3. tính tiền và khối lượng của chi  tiết  phức ,  máy và kho.
4. Xây dựng class helper.
5. Hiển thị thông tin kho, máy kiểu table.
6. Validate các dữ liệu nhập vào.

* --- Check list ---
1. validate mã số (chi tiết, máy, kho):
   - không được để trống
   - không được nhập ký tự đặc biệt
2. validate tên (máy, kho):
   - không được để trống
   - không được nhập ký tự đặc biệt
3. validate trọng lượng, khối lượng (chi tiết đơn)
   - không được nhập nhỏ hơn 0 (số âm)
   - không được để trống
   - chỉ được nhập ký tự số
4. validate số lượng (chi tiết, máy)
   - không được nhập nhỏ hơn 0 (số âm)
   - không được để trống
   - chỉ được nhập ký tự số
5. validate lựa chọn (loại chi tiết, chức năng ở index)
   - không được để trống
   - chỉ được nhập ký tự số
   - không được nhập số thực
   - chỉ được nhập trong khoảng 1 - 3 đối với lựa chọn ở index, 1 - 2 đối với loại chi tiết 
 

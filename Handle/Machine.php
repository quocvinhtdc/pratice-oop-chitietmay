<?php
include_once 'Complex.php';
include_once 'Helper/ConsoleTable.php';

Class Machine{

    /**
     * @Var int
     * mã số máy
     */
    public $idMachine;

    /**
     * @var string
     * tên máy
     */
    public $nameMachine;

    /**
     * @Array array
     * danh sách chi tiết máy
     */
    public $listDetailMachine = array();

    /**
     * @var int
     * số lượng chi tiết trong máy
     */
    public $numberOfDetail;

    /**
     * @var array
     * thông tin của máy
     */
    public $inFoMachine = array();

    // tính tổng khối lượng
    public function TotalWeightMachine() {
        $totalWeight = 0;
        foreach ($this->listDetailMachine as $machine) {
            if (isset($machine->weight)) { // nếu có trọng lượng thì nó sẽ là thằng ct đơn
                $totalWeight += $machine->TotalWeight();
            }
            else {
                $totalWeight += $machine->TotalWeight();
            }
        }
        return $totalWeight;
    }

    // tính tổng giá của các máy
    public function TotalPriceMachine() {
        $totalPrice = 0;
        foreach ($this->listDetailMachine as $machine) {
            if (isset($machine->weight)) { // nếu có trọng lượng thì nó sẽ là thằng ct đơn
                $totalPrice += $machine->TotalPrice();
            }
            else {
                $totalPrice += $machine->TotalPrice();
            }
        }
        return $totalPrice;
    }

    // nhập thông tin cho máy
    public function Input() {
        echo "\t\t---WELLCOME---\n";
            echo " -----  Mời Bạn Nhập Thông Tin Cho Máy  -----\n";

            $messId = "Nhập mã máy: \t";
            $this->idMachine = Helper::validateText($this->idMachine, $messId);

            $messName = "Nhập tên máy: ";
            $this->nameMachine = Helper::validateText($this->nameMachine, $messName);

            $messInput = "Nhập số lượng chi tiết của máy: ";
            $numberOfDetail = Helper::validateInput($this->numberOfDetail, $messInput);

            $this->inFoMachine["idMachine"] = $this->idMachine;
            $this->inFoMachine["nameMachine"] = $this->nameMachine;
            $this->inFoMachine["sizeOfMachine"] = $numberOfDetail;

            for($i = 1; $i <= $numberOfDetail; $i++) {
                echo "------------------------------------\n";
                echo " - Nhập thông tin chi tiết thứ $i  -\n";
                echo "------------------------------------\n\t";
                $this->listDetailMachine["Detail-$i"] = Helper::itemDetailType();
            }
    }

    // xuất thông tin máy
    public function InfoMachine() {
        echo "\n====== THÔNG TIN CỦA MÁY ====== \n";
        $tbl = new ConsoleTable();
        $tbl->setHeaders(array("Mã Máy", "Tên Máy", "SL Chi Tiết"));
        $tbl->addRow(array(
            $this->idMachine,
            $this->nameMachine,
            sizeOf($this->listDetailMachine)
        ));
        echo $tbl->getTable();
    }

    // xuất thông tin của chi tiết máy
    public function Output() {
      $this->InfoMachine();
        echo "\n DANH SÁCH CÁC CHI TIẾT CỦA MÁY \n";
        var_dump($this->listDetailMachine);
        echo "\n Tổng Khối lượng máy: ".$this->TotalWeightMachine()."kg";
        echo "\n Tổng Tiền của máy: ".$this->TotalPriceMachine()."$";
    }
}
<?php
include_once 'Detail.php';
include_once 'Helper/Helper.php';

Class Single extends Detail{
    /**
     * @var float
     */
    public $price;

    /**
     * @var float
     */
    public $weight;

    public function Input() {
        parent::Input();
        $messInput = "Nhập trọng lượng: ";
        $this->weight = Helper::validateNumber($this->weight, $messInput);

        $messInputPrice = "Nhập giá: ";
        $this->price = Helper::validateNumber($this->price, $messInputPrice);
    }

    public function TotalPrice() {
        return $this->price;
    }

    public function TotalWeight() {
        return $this->weight;
    }
}

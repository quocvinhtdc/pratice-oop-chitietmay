<?php
include_once 'Detail.php';
include_once 'Single.php';
include_once 'Helper/Helper.php';

Class Complex extends Detail
{
    /**
     * @var array
     * danh sach chi tiet con
     */
    public $listDetail = array();

    /**
     * @var int
     * so luong chi tiet
     */
    public $numberOf;

    // nhập thông tin chi tiết phức
    public function Input()
    {
        $messInput = "Nhập số lượng chi tiết con: ";
        $this->numberOf = Helper::validateInput($this->numberOf, $messInput);
        for ($i = 1; $i <= $this->numberOf; $i++) {
            echo "------------------------------------\n";
            echo " - Nhập thông tin chi tiết con thứ $i  -\n";
            echo "------------------------------------\n\t";
            parent::Input();
            $this->listDetail["Complex$i"] =  Helper::itemDetailType();
        }
    }

    // xuất thông tin chi tiết phức
    public function Output()
    {
        var_dump($this->listDetail);
    }

    // tính tổng tiền
    public function TotalPrice()
    {
        $totalPrice = 0;
        foreach ($this->listDetail as $detail) {
            if (isset($detail->weight)) { // nếu có trọng lượng thì nó sẽ là thằng ct đơn
                $totalPrice += $detail->TotalPrice();
            }
            else {
                $totalPrice += $detail->TotalPrice();
            }
        }
       return $totalPrice;
    }

    // tính tổng khối lượng
    public function TotalWeight()
    {
        $totalWeight = 0;
        foreach ($this->listDetail as $detail) {
            if (isset($detail->weight)) { // nếu có trọng lượng thì nó sẽ là thằng ct đơn
                $totalWeight += $detail->TotalWeight();
            }
            else {
                $totalWeight += $detail->TotalWeight();
            }
        }
        return $totalWeight;
    }
}

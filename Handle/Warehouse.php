<?php
include_once 'Complex.php';
include_once 'Machine.php';
include_once  'Helper/ConsoleTable.php';

Class Warehouse{
    /**
     * @var array
     * thông tin của kho
     */
    public $infoOfWareHouse = array();

    /**
     * @var  array
     * danh sách các máy
     */
    public $listMachine = array();

    /**
     * @var  int
     * ma so kho
     */
    public $idWarehouse;

    /**
     * @var string
     * tên kho
     */
    public $nameWarehouse;

    /**
     * @var int
     * số lượng máy trong kho
     */
    public $numberOfMachine;

    /**
     * @var int
     * số lượng máy trong kho
     */
    public $sizeOf; // sức chứa của kho

    // tính tổng khối lượng
    public function TotalWeightOfListMachine() {
        $totalWeight = 0;
        foreach ($this->listMachine as $machine){
            $totalWeight += $machine->TotalWeightMachine();
        }
        return $totalWeight;
    }

    // tính tổng giá của các máy có trong kho
    public function TotalPriceOfListMachine() {
        $totalPrice = 0;
        foreach ($this->listMachine as $machine){
            $totalPrice += $machine->TotalPriceMachine();
        }

        return $totalPrice;
    }

    // nhập thông tin cho 1 máy
    public function Input() {
        echo "\t\t---WELLCOME---\n";
            echo " -----  Mời Bạn Nhập Thông Tin Cho Kho  -----\n";
            $messId = "Nhập Mã kho: \t";
            $this->idWarehouse = Helper::validateText($this->idWarehouse ,$messId);

            $messName = "Nhập tên kho: ";
            $this->nameWarehouse = Helper::validateText($this->nameWarehouse, $messName);

            $messSize = "Nhập vào sức chứa của kho: ";
            $this->sizeOf = Helper::validateInput($this->sizeOf, $messSize);

            do {
                $messInput = "Nhập số lượng máy có trong kho: "; // nếu nhập quá sức chứa sẽ bị lỗi
                $numberOfMachine = Helper::validateInput($this->numberOfMachine, $messInput);
                if($this->sizeOf < $numberOfMachine) {
                    echo "Lỗi!!! Số máy không được vượt quá sức chứa của kho! Vui lòng nhập lại \n";
                }

                $this->infoOfWareHouse["idWareHouse"] = $this->idWarehouse;
                $this->infoOfWareHouse["nameWareHouse"] = $this->nameWarehouse;
                $this->infoOfWareHouse["sizeOfWareHouse"] = $this->sizeOf;
                $this->infoOfWareHouse["sizeOfWareHouse"] =$numberOfMachine;

            }while($this->sizeOf < $numberOfMachine);

            for($i = 1; $i <= $numberOfMachine; $i++) {
                echo "------------------------------------\n";
                echo " - Nhập thông tin của máy thứ $i  -\n";
                echo "------------------------------------\n\t";
                $machine = new Machine();
                $machine->Input();
                array_push($this->listMachine, $machine);
            }
    }

    // xuất thông tin kho
    public function InfoWarehouse() {
        echo "\n====== THÔNG TIN CỦA KHO ====== \n";
        $tbl = new ConsoleTable();
        $tbl->setHeaders(array("Mã Kho", "Tên Kho", "Sức Chứa", "Số lượng máy"));
        $tbl->addRow(array(
            $this->idWarehouse,
            $this->nameWarehouse,
            $this->sizeOf,
            sizeOf($this->listMachine)
        ));
        echo $tbl->getTable();
    }

    // xuất thông tin máy
    public function Output() {
        $this->InfoWarehouse();
        echo "\n============= DANH SÁCH CÁC MÁY TRONG KHO ===========\n";
        var_dump($this->listMachine);
        echo "\n Tổng Khối lượng máy có trong kho: ".$this->TotalWeightOfListMachine()."kg";
        echo "\n Tổng Tiền của máy có trong  kho: ".$this->TotalPriceOfListMachine()."$\n";
    }
}
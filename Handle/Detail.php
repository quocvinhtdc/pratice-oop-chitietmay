<?php
include_once "Helper/Helper.php";

Abstract Class Detail{
    /**
     * @var string
     * mã số chi tiết
     */
    public $maSo;

    // lấy mã số
    public function getMaSo() {
        return $this->maSo;
    }

    // nhập mã số chi tiết
    public function Input(){
        $messInput = "Nhập mã số: ";
        $this->maSo = Helper::validateText($this->maSo, $messInput);
    }

    // xuất mã số chi tiết
    public function Output() {
        echo $this->maSo;
    }

    // tính tổng tiền
   public Abstract function TotalPrice();

    // tính tổng khối lượng
    public Abstract function TotalWeight();
}

<?php
include_once "Helper/Helper.php";
include_once "Handle/Machine.php";
include_once "Handle/Warehouse.php";

     /**
     * @var int
     * lựa chọn chức năng
     */
    $nChoises = 0;

    // hiển thị kiểu bảng
    $tbl = new ConsoleTable();
    $tbl->setHeaders(array("Lựa Chọn Chức Năng"));
    $tbl->addRow(array("1. Nhập thông tin kho"));
    $tbl->addRow(array("2. Nhập thông tin máy"));
    $tbl->addRow(array("3. Thoát"));
    echo $tbl->getTable();

    do {
        $messInput = "Mời Bạn Lựa Chọn: ";

        $nChoises = Helper::validateInput($nChoises, $messInput);
        if($nChoises > 3 || $nChoises <= 0){
            echo "Lỗi!!! lựa chọn không hợp lệ \n";
        }
        switch ($nChoises) {
            case 1:
            {
                $warehouse = new Warehouse();
                $warehouse->Input();
                $warehouse->Output();
                break;
            }
            case 2:
            {
                $machine = new Machine();
                $machine->Input();
                $machine->Output();
                break;
            }
            case 3:
            {
                die("Bye bye, See you!!! \n");
            }
            default:
            {
                echo "Xin vui lòng chọn lại !!!";
            }
        }
    }while($nChoises > 3 || $nChoises <= 0);



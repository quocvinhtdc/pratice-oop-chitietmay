<?php
include_once 'Handle/Complex.php';
include_once 'Handle/Single.php';

Class Helper {

    // kiểm tra loại chi tiết
    public static function itemDetailType() {
            $item = null;
            do {
                $type = readline("Nhập Loại chi tiết con (1-Single - 2-Complex): ");
                if ($type == 1) {
                    $item = new Single();
                } else if ($type == 2) {
                    echo "\n==== Nhập số thông tin chi tiết con trong chi tiết phức === \n\t";
                    $item = new Complex();
                } else {
                    echo "\nLựa chọn không hợp lệ! Vui lòng nhập lại \n";
                }
            } while ($type != 1 && $type != 2);
            $item->Input();
            return $item;
    }

    // kiểm tra thông tin nhập vào có phải là số
    public static function validateInput($param ,$messInput) {
        do {
            $paramInput = readline("$messInput");
            // convert về kiểu số
            if(is_numeric($paramInput)) {
                $paramInput += 0;
            }
            if(!is_numeric($paramInput)) {
               echo "Lỗi !!! Nhập vào phải là một số, Xin vui lòng nhập lại \n";
            }
            elseif (!is_int($paramInput)) {
                echo "Lỗi !!! Nhập vào phải là số nguyên. \n";
            }
            elseif ($paramInput <= 0 ) {
                echo "Lỗi !!! Nhập vào phải lớn hơn 0 \n";
            }
            elseif ($paramInput == '' ) {
                echo "Lỗi !!! Không được để trống \n";
            }
            else {
                $param = $paramInput;
            }
        }while(!is_numeric($paramInput) || $paramInput == '' || $paramInput <= 0 || !is_int($paramInput));
        return $param;
    }

    // kiểm tra text nhập vào
    public static function validateText($param,$messInput) {
        do {
            $pattern = "/([\|\+()=@^&!%\$#\*]+)/";
            $paramInput = readline("$messInput");
            if($paramInput == '') {
                echo "Lỗi!!! Không được để trống, Vui lòng nhập lại \n";
            }else if (preg_match($pattern, $paramInput )) {
                echo "Lỗi!!! Không được chứa ký tự đặc biệt, Vui lòng nhập lại \n";
            }
            else {
                $param = $paramInput;
            }
        }while($paramInput == '' || preg_match($pattern, $paramInput ));
        return $param;
    }

    // kiểm tra cho phép nhập số thực
    public static function validateNumber($param ,$messInput) {
        do {
            $paramInput = readline("$messInput");

            if(!is_numeric($paramInput)) {
                echo "Lỗi !!! Nhập vào phải là một số, Xin vui lòng nhập lại \n";
            }
            elseif ($paramInput <= 0 ) {
                echo "Lỗi !!! Nhập vào phải lớn hơn 0 \n";
            }
            elseif ($paramInput == '' ) {
                echo "Lỗi !!! Không được để trống \n";
            }
            else {
                $param = $paramInput;
            }
        }while(!is_numeric($paramInput) || $paramInput == '' || $paramInput <= 0);
        return $param;
    }
}